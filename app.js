/*
Теоретичні питання:
1. Опишіть, як можна створити новий HTML тег на сторінці.
  Створюємо тег HTML через createElement:
  const divEl = document.createElement("div");
  Контент всередині тега створюємо через властивість об'єкта (тега) innerHTML:
  divEl.innerHTML = "Hello World!";
  Щоб тег показався, нам потрібно вставити його десь на сторінці в document. Наприклад, в елемент body через метод вставки append:
  document.body.append(divEl);
2. Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра.
  Щоб вставити рядок HTML "як HTML" з усіма тегами, використовуємо метод insertAdjacentHTML("перший параметр", "другий параметр").
  Перший параметр вказує куди вставляємо код прописаний рядком у другому параметрі. Існує чотири кодові слова для першого параметру:
  - "beforebegin" - вставляє перед елементом, до якого застосовується метод;
  - "afterbegin" - вставляє в елемент на початку;
  - "beforeend" - вставляє в елемент в кінці;
  - "afterend" - вставляє після елементу.
  divEl.insertAdjacentHTML("beforebegin", "<div><b>Hello</b></div>");
3. Як можна видалити елемент зі сторінки?
  Видалити елемент можна за допомогою методу remove():
  divEl.remove();
*/

/*
Реалізувати функцію, яка отримуватиме масив елементів і виводити їх на сторінку у вигляді списку. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

Технічні вимоги:
Створити функцію, яка прийматиме на вхід масив і опціональний другий аргумент parent - DOM-елемент, до якого буде прикріплений список (по дефолту має бути document.body.
кожен із елементів масиву вивести на сторінку у вигляді пункту списку;
Приклади масивів, які можна виводити на екран:

["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
["1", "2", "3", "sea", "user", 23];
Можна взяти будь-який інший масив.
*/

let myArray = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
let myArray1 = ["1", "2", "3", "sea", "user", 23];

function getMyList (array, el = document.body) {
  let ul = document.createElement("ul");
  array.forEach(item => {
    const li = document.createElement("li");
    li.textContent = item;
    ul.append(li);
  });
  el.append(ul);
}

getMyList(myArray);
getMyList(myArray1);

